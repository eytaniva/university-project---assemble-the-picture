package LogicLayer;

import javax.swing.*;
import java.awt.*;

public class BoardButton extends JButton {
    private JButton pic = null;
    private boolean isEmpty;
    private int row, col;
    private String index;

    public BoardButton(String strng, int row, int col, String index){
        super(strng);
        this.row = row;
        this.col = col;
        this.isEmpty = false;
        this.index = index;
    }

    public BoardButton(int row, int col, String index){
        super("");
        this.row = row;
        this.col = col;
        this.isEmpty = false;
        this.index = index;
    }
    public BoardButton(JButton pic, int row, int col, String index){
        this.pic = pic;
        this.setIcon(this.pic.getIcon());
        this.row = row;
        this.col = col;
        this.isEmpty = false;
        this.index = index;

    }

    public void setIsEmpty(boolean empty){
        this.isEmpty = empty;
    }

    public boolean getIsEmpty(){
        return this.isEmpty;
    }

    public void setRow(int row){
        this.row = row;
    }

    public int getRow(){
        return this.row;
    }

    public void setCol(int col){
        this.col = col;
    }

    public int getCol(){
        return this.col;
    }

    public String getIndex() {
        return this.index;
    }
}
