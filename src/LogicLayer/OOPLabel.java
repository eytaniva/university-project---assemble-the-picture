package LogicLayer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.Random;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.Timer;

import java.util.*;

public class OOPLabel extends JLabel {
	//fields
	private final Font myFont = new Font("Console", Font.ITALIC, 36);
	
	//Constructor
//	public OOPLabel(JFrame frame) {
//		super();
//		frame.add(this);
//	}

	public OOPLabel(JFrame frame, String s) {
		super(s);
		this.setFont(myFont);
		frame.add(this);
	}

}
