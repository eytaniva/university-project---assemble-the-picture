package LogicLayer;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class CSVReader {
	//Fields
	private String path;

	//Constructor
	public CSVReader(String path){
		this.path = path;
	}

    public  LinkedList<LinkedList<String[][]>> CSVToBoards() {
		LinkedList<String[][]> board3 = new LinkedList<String[][]>();
		LinkedList<String[][]> board4 = new LinkedList<String[][]>();
		LinkedList<String[][]> board5 = new LinkedList<String[][]>();
        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader(this.path))) {

            while ((line = br.readLine()) != null) {
        		if (line.equals("3")) {
        			String[][] board = new String[3][3];
        			for (int i = 0; i <= 2; i++) {
        				line = br.readLine();
        				String[] numbers = line.split(cvsSplitBy);
        				for (int j = 0; j <= 2; j++) {
							board[i][j] = numbers[j];
            			}
        			}
					board3.add(board);
        		}

        		if (line.equals("4")) {
        			String[][] board = new String[4][4];
        			for (int i = 0; i <= 3; i++) {
        				line = br.readLine();
        				String[] numbers = line.split(cvsSplitBy);
        				for (int j = 0; j <= 3; j++) {
							board[i][j] = numbers[j];
            			}
        			}
					board4.add(board);
        		}

        		if (line.equals("5")) {
        			String[][] board = new String[5][5];
        			for (int i = 0; i <= 4; i++) {
        				line = br.readLine();
        				String[] numbers = line.split(cvsSplitBy);
        				for (int j = 0; j <= 4; j++) {
							board[i][j] = numbers[j];
            			}
        			}
					board5.add(board);
        		}
            }

        } catch (IOException e) {
        	final Font myFont = new Font("Console", Font.ITALIC, 36);
			JLabel label = new JLabel("Something went wrong");
			label.setFont(myFont);
			JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);

        }
		LinkedList<LinkedList<String[][]>> allboards = new LinkedList<LinkedList<String[][]>>();
		allboards.add(board3);
		allboards.add(board4);
		allboards.add(board5);
        return allboards;
    }
}