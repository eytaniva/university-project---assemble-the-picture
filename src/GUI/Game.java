package GUI;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import LogicLayer.*;
import java.util.Stack;
import java.util.Random;


public class Game extends JFrame implements ActionListener, KeyListener {

	//-----------------Fields----------------------------------------------------------------------------------------------
	private BoardButton[][] board;
	private String[] completeBoard;
	private final int sizeTimes = 2;
	private final int WIDTH=900* sizeTimes;
	private final int HEIGHT=1000* sizeTimes;
	private int timerHours = 0, timerMinutes = 0, timerSeconds = 0;
	private OOPLabel stepsLabel;
	private int stepsCounter;
	private Timer timer;
	private boolean timerSecondsTwoDigits, timerMinutesTwoDigits, timerHoursTwoDigits;
	private String timerSecondsString, timerMinutesString, timerHoursString, finishTime;
	private final int SPEED = 1000;//the timer will run every 1000 milliseconds
	private int boardSize ;
	private int boardCellSize;
	private int emptyCellRow;
	private int emptyCellCol;
	private static OOPLabel timeLabel;
	private static MigLayout layout;
	private String pic;
	private JButton undoButton;
	private Stack<BoardButton> undoStack;
	private Random rand;
	private CSVReader csv = new CSVReader("src\\LogicLayer\\boards.csv");


	//-----------------Constructor----------------------------------------------------------------------------------------------
	public Game(int boardSize, String imagePath) {
		super("Sliding Puzzle");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.boardSize = boardSize;
		this.layout = new MigLayout();
		this.setLayout(this.layout);
		this.pic = imagePath;
		this.rand = new Random();
		this.completeBoard = new String[this.boardSize * this.boardSize];

		this.timerSecondsTwoDigits = false;
		this.timerMinutesTwoDigits = false;
		this.timerHoursTwoDigits = false;
		this.timerSecondsString = null;
		this.timerMinutesString = null;
		this.timerHoursString = null;
		this.timeLabel = new OOPLabel(this,"Time: 00:00:00");
		initTimer();

		this.stepsCounter = 0;
		this.stepsLabel = new OOPLabel(this,"Steps: " + this.stepsCounter);
		this.add(this.stepsLabel);
		this.layout.setComponentConstraints(this.stepsLabel, "pos 1000px 25px");

		initUndoButton();

		this.boardSize = boardSize;
		this.boardCellSize = (this.boardSize == 3) ? (this.WIDTH/3) : ((this.boardSize == 4) ? (this.WIDTH/4) : (this.WIDTH/5));
		this.board = new BoardButton[boardSize][boardSize];
		initBoard(this.board, imagePath);

		initCompleteBoard();

		setFocusable(true);
		requestFocusInWindow();
		addKeyListener(this);
		setSize(WIDTH, HEIGHT);
		this.getContentPane().setBackground(Color.cyan);
		setVisible(true);
	}

	//-----------------Privates------------------------------------------------------------------------------------------------
	private void initTimer(){
		timer = new Timer(SPEED, this);
		timer.setInitialDelay(SPEED);
		this.add(this.timeLabel);
		this.timeLabel.setVisible(true);
		this.layout.setComponentConstraints(this.timeLabel, "pos 50px 25px");
		timer.start();
	}

	private void updateTime(){
		timerSeconds++;
		this.timerSecondsTwoDigits = (this.timerSeconds > 9);
		this.timerMinutesTwoDigits = (this.timerMinutes > 9);
		this.timerHoursTwoDigits = (this.timerHours > 9);
		if(timerSeconds > 59){
			timerSeconds = 0;
			timerMinutes++;
			if(timerMinutes > 59){
				timerMinutes = 0;
				timerHours++;
			}
		}
		this.timerSecondsString = (this.timerSecondsTwoDigits) ? ("" + this.timerSeconds) : ("0" + this.timerSeconds);
		this.timerMinutesString = (this.timerMinutesTwoDigits) ? ("" + this.timerMinutes) : ("0" + this.timerMinutes);
		this.timerHoursString = (this.timerHoursTwoDigits) ? ("" + this.timerHours) : ("0" + this.timerHours);
		this.timeLabel.setText("Time: " + timerHoursString + ":" + timerMinutesString + ":" + timerSecondsString);
		this.finishTime = this.timeLabel.getText();
	}

	private void initUndoButton(){
		final Font myFont = new Font("Console", Font.ITALIC, 36);
		this.undoButton = new JButton("Undo");
		this.undoButton.setFont(myFont);
		this.undoButton.setPreferredSize(new Dimension(50,50));
		this.add(this.undoButton);
		this.layout.setComponentConstraints(this.undoButton, "pos 1500px 25px");
		this.undoStack = new Stack<BoardButton>();
		this.undoButton.addActionListener(this);
	}

	private void undoBoard(){
		try {
			setCell(undoStack.pop());
		}
		catch(Exception e){
			final Font myFont = new Font("Console", Font.ITALIC, 36);
			JLabel label = new JLabel("No more moves to be undone");
			label.setFont(myFont);
			JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);
		}

	}

	private void setCell(BoardButton tmpBtn){
		try {
			exchangeButtons(tmpBtn, this.board[this.emptyCellRow][this.emptyCellCol]);
			undoStack.pop();
		}
		catch(Exception e){
			//Already handled
		}
	}

	private void initBoard(BoardButton[][] board, String s) {
		try {
			SetBoard(csv.CSVToBoards());
			for(int i=0; i <= board.length - 1; i++) {
				for(int j=0; j <= board.length - 1; j++) {
					this.add(this.board[i][j]);
					this.layout.setComponentConstraints(this.board[i][j], "pos " + (j * this.boardCellSize) + "px " + (100 + i * this.boardCellSize) + "px");
					this.board[i][j].setPreferredSize(new Dimension(boardCellSize, boardCellSize));
					this.board[i][j].setVisible(true);
					this.board[i][j].addActionListener(this);
				}
			}
		}
		catch (Exception ex) {
			final Font myFont = new Font("Console", Font.ITALIC, 36);
			JLabel label = new JLabel("Wrong image path");
			label.setFont(myFont);
			JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);
		}
	}
	private void SetBoard(LinkedList<LinkedList<String[][]>> iWillMakeThisBoard){
		LinkedList<String[][]> chooseSize = iWillMakeThisBoard.get(boardSize % 3);
		String[][] almostBoard = chooseSize.get(randomBoard(iWillMakeThisBoard));
		for(int i=0; i<=boardSize-1; i++){
			for(int j=0; j<=boardSize-1; j++){
				try {
					int name = Integer.parseInt(almostBoard[i][j]);
					if(name == 0){
						name = ((this.boardSize * this.boardSize) - 1);
					}
					String imageAllPath = (setImagePath(this.pic, board.length) + (name) + setJPEG());
					this.board[i][j] = new BoardButton(BuildOOPButton(imageAllPath), i, j, almostBoard[i][j] + "");
					this.board[i][j].setBorder(BorderFactory.createEmptyBorder());
					if(this.board[i][j].getIndex().equals(0 + "")){
						this.board[i][j].setIcon(null);
						this.board[i][j].setIsEmpty(true);
						this.emptyCellRow = i;
						this.emptyCellCol = j;
					}
				}
				catch (Exception e){
					final Font myFont = new Font("Console", Font.ITALIC, 36);
					JLabel label = new JLabel("Something went wrong");
					label.setFont(myFont);
					JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private int randomBoard(LinkedList<LinkedList<String[][]>> iWillMakeThisBoard){
		LinkedList<String[][]> chooseSize = iWillMakeThisBoard.get(boardSize % 3);
		int n = rand.nextInt(chooseSize.size());
		return n;
	}


	private String setImagePath(String s,int n){
		return s + "\\" + s + "_" + n + "x" + n + "\\";
	}

	private String setJPEG(){
		return  ".jpeg";
	}

	private JButton BuildOOPButton(String strng){//this is only for checking
		Image img = null;
		String imgPath = "\\PicturesFile\\" + strng;//setImagePath(this.pic,board.length) + count + setJPEG()
		//String allPath = getClass().getResource(imgPath).getPath();
		try{
			img = ImageIO.read(getClass().getResource(imgPath));
		}
		catch(IOException e){
			final Font myFont = new Font("Console", Font.ITALIC, 36);
			JLabel label = new JLabel("Something went wrong");
			label.setFont(myFont);
			JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);
		}
		img = img.getScaledInstance(boardCellSize, boardCellSize, Image.SCALE_SMOOTH);//(boardCellSize, boardCellSize)
		JButton btn = new JButton();
		btn.setIcon(new ImageIcon(img));
		return btn;
	}

	private void initCompleteBoard(){
		int counter = 1;
		for (int i = 0; i <= this.boardSize - 1; i++){
			for (int j = 0; j <= this.boardSize - 1; j++){
				if(counter == (this.boardSize * this.boardSize) /*(i == this.boardSize - 1) & (j == this.boardSize - 1)*/){
					this.completeBoard[(this.boardSize * this.boardSize) - 1] = "0";
				}
				else{
					this.completeBoard[counter-1] = counter + "";
				}
				counter++;
			}
		}
	}

	private void exchangeButtons(BoardButton clickedBtn, BoardButton emptyBtn){
		int tmpRow = clickedBtn.getRow(), tmpCol = clickedBtn.getCol();
		clickedBtn.setRow(emptyBtn.getRow());
		clickedBtn.setCol(emptyBtn.getCol());
		emptyBtn.setRow(tmpRow);
		emptyBtn.setCol(tmpCol);
		this.emptyCellRow = emptyBtn.getRow();
		this.emptyCellCol = emptyBtn.getCol();
		this.layout.setComponentConstraints(clickedBtn, "pos " + (clickedBtn.getCol() * this.boardCellSize) + "px " + (100 + clickedBtn.getRow() * this.boardCellSize) + "px");
		this.layout.setComponentConstraints(emptyBtn, "pos " + (emptyBtn.getCol() * this.boardCellSize) + "px " + (100 + emptyBtn.getRow() * this.boardCellSize) + "px");
		this.board[clickedBtn.getRow()][clickedBtn.getCol()] = clickedBtn;
		this.board[emptyBtn.getRow()][emptyBtn.getCol()] = emptyBtn;
		this.requestFocus(true);
		this.undoStack.push(clickedBtn);
		checkCompletion();
	}

	private void checkCompletion(){
		int counter = 0;
		boolean isCompleted = true;
		for(int i = 0; (i <= this.boardSize - 1) & isCompleted; i++){
			for(int j = 0; (j <= this.boardSize - 1) & isCompleted; j++){
				if(!(this.board[i][j].getIndex().equals(this.completeBoard[counter]))){
					isCompleted = false;
				}
				counter++;
			}
		}
		if(isCompleted){
			Image img = null;
			String imgPath = "\\PicturesFile\\" + this.pic + "\\" + this.pic + "_"
					+ this.boardSize + "x" + this.boardSize + "\\" + (this.boardSize*this.boardSize) + setJPEG();  //setImagePath(this.pic,board.length) + count + setJPEG()
			String allPath = getClass().getResource(imgPath).getPath();
			try{
				img = ImageIO.read(getClass().getResource(imgPath));
			}
			catch(IOException e){
				final Font myFont = new Font("Console", Font.ITALIC, 36);
				JLabel label = new JLabel("Something went wrong");
				label.setFont(myFont);
				JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);
			}
			img = img.getScaledInstance(boardCellSize, boardCellSize, Image.SCALE_SMOOTH);//(boardCellSize, boardCellSize)
			this.board[boardSize-1][boardSize-1].setIcon(new ImageIcon(img));
			new Victory(this, this.stepsCounter, this.finishTime);
		}

	}

	//--------------------------------------------------------------ActionPerformed-------------------------------------------------
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource().equals(this.timer)){
			updateTime();
		}
		else if(event.getSource() instanceof BoardButton) {//when a button from the board gets pressed
			BoardButton tmpBtn = (BoardButton) event.getSource();
			int rowOfClicked = tmpBtn.getRow();
			int colOfClicked = tmpBtn.getCol();
			if ((rowOfClicked > 0) & (rowOfClicked < this.boardSize - 1) & (colOfClicked > 0) & (colOfClicked < this.boardSize - 1)) {//if the clicked button is not on the perimeter of the board
				if (this.board[rowOfClicked - 1][colOfClicked].getIsEmpty()) {//if the empty button is above the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked - 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked + 1][colOfClicked].getIsEmpty()) {//if the empty button is below the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked + 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked + 1].getIsEmpty()) {//if the empty button is to the right of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked + 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked - 1].getIsEmpty()) {//if the empty button is to the left of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked - 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((rowOfClicked == 0) & (colOfClicked > 0) & (colOfClicked < this.boardSize - 1)) {//if the clicked button is in the top row but not in the corners
				if (this.board[rowOfClicked + 1][colOfClicked].getIsEmpty()) {//if the empty button is below the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked + 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked + 1].getIsEmpty()) {//if the empty button is to the right of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked + 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked - 1].getIsEmpty()) {//if the empty button is to the left of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked - 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((rowOfClicked == this.boardSize - 1) & (colOfClicked > 0) & (colOfClicked < this.boardSize - 1)) {//if the clicked button is in the bottom row but not in the corners
				if (this.board[rowOfClicked - 1][colOfClicked].getIsEmpty()) {//if the empty button is above the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked - 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked + 1].getIsEmpty()) {//if the empty button is to the right of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked + 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked - 1].getIsEmpty()) {//if the empty button is to the left of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked - 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((colOfClicked == 0) & (rowOfClicked > 0) & (rowOfClicked < this.boardSize - 1)) {//if the clicked button is in the left column but not in the corners
				if (this.board[rowOfClicked - 1][colOfClicked].getIsEmpty()) {//if the empty button is above the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked - 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked + 1][colOfClicked].getIsEmpty()) {//if the empty button is below the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked + 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked + 1].getIsEmpty()) {//if the empty button is to the right of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked + 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((colOfClicked == this.boardSize - 1) & (rowOfClicked > 0) & (rowOfClicked < this.boardSize - 1)) {//if the clicked button is in the right column but not in the corners
				if (this.board[rowOfClicked - 1][colOfClicked].getIsEmpty()) {//if the empty button is above the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked - 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked + 1][colOfClicked].getIsEmpty()) {//if the empty button is below the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked + 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked - 1].getIsEmpty()) {//if the empty button is to the left of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked - 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((rowOfClicked == 0) & (colOfClicked == 0)) {//if the clicked button is in the top left corner
				if (this.board[rowOfClicked + 1][colOfClicked].getIsEmpty()) {//if the empty button is below the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked + 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked + 1].getIsEmpty()) {//if the empty button is to the right of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked + 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((rowOfClicked == 0) & (colOfClicked == this.boardSize - 1)) {//if the clicked button is in the top right corner
				if (this.board[rowOfClicked + 1][colOfClicked].getIsEmpty()) {//if the empty button is below the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked + 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked - 1].getIsEmpty()) {//if the empty button is to the left of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked - 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((rowOfClicked == this.boardSize - 1) & (colOfClicked == 0)) {//if the clicked button is in the bottom left corner
				if (this.board[rowOfClicked - 1][colOfClicked].getIsEmpty()) {//if the empty button is above the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked - 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked + 1].getIsEmpty()) {//if the empty button is to the right of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked + 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
			else if ((rowOfClicked == this.boardSize - 1) & (colOfClicked == this.boardSize - 1)) {//if the clicked button is in the bottom right corner
				if (this.board[rowOfClicked - 1][colOfClicked].getIsEmpty()) {//if the empty button is above the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked - 1][colOfClicked]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
				else if (this.board[rowOfClicked][colOfClicked - 1].getIsEmpty()) {//if the empty button is to the left of the clicked button
					exchangeButtons(tmpBtn, this.board[rowOfClicked][colOfClicked - 1]);
					this.stepsCounter++;
					this.stepsLabel.setText("Steps: " + this.stepsCounter);
				}
			}
		}
		else if (event.getSource().equals(this.undoButton)) {
			undoBoard();
			this.requestFocus(true);
		}
	}

	//-----------------Publics----------------------------------------------------------------------------------------------
	@Override
	public void keyTyped(KeyEvent event) {
		//Nothing happens
	}

	@Override
	public void keyPressed(KeyEvent event) {
		//Nothing happens
	}

	@Override
	public void keyReleased(KeyEvent event) {
		if ((event.getKeyCode() == KeyEvent.VK_Z) && ((event.getModifiers() & KeyEvent.CTRL_MASK) != 0)){
			undoBoard();
		}
		else if (event.getKeyCode() == KeyEvent.VK_UP) {
			if((this.emptyCellRow < this.boardSize - 1)){
				exchangeButtons(this.board[this.emptyCellRow + 1][this.emptyCellCol], this.board[this.emptyCellRow][this.emptyCellCol]);
				this.stepsCounter++;
				this.stepsLabel.setText("Steps: " + this.stepsCounter);
			}
		}
		else if (event.getKeyCode() == KeyEvent.VK_DOWN){
			if((this.emptyCellRow > 0)){
				exchangeButtons( this.board[this.emptyCellRow - 1][this.emptyCellCol], this.board[this.emptyCellRow][this.emptyCellCol]);
				this.stepsCounter++;
				this.stepsLabel.setText("Steps: " + this.stepsCounter);
			}
		}
		else if (event.getKeyCode() == KeyEvent.VK_RIGHT){
			if((this.emptyCellCol > 0)){
				exchangeButtons(this.board[this.emptyCellRow][this.emptyCellCol - 1], this.board[this.emptyCellRow][this.emptyCellCol]);
				this.stepsCounter++;
				this.stepsLabel.setText("Steps: " + this.stepsCounter);
			}
		}
		else if (event.getKeyCode() == KeyEvent.VK_LEFT){
			if((this.emptyCellCol < this.boardSize - 1)){
				exchangeButtons(this.board[this.emptyCellRow][this.emptyCellCol + 1], this.board[this.emptyCellRow][this.emptyCellCol]);
				this.stepsCounter++;
				this.stepsLabel.setText("Steps: " + this.stepsCounter);
			}
		}
	}
}
