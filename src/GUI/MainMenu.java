package GUI;

import LogicLayer.*;
import net.miginfocom.swing.MigLayout;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.LinkedList;

public class MainMenu extends JFrame implements ActionListener {

    //-----------------Fields---------------------------------------------------------------------------------------------
    private JButton startButton, exitButton;
    private JButton board_3x3, board_4x4, board_5x5;
    private JButton catButton;
    private JButton cyberButton, sushiButton;
    private OOPLabel mainMenuLabel, imageChoiceLabel, boardChoiceLabel;
    private String pic;
    private final int frame_WIDTH = 100000, frame_HEIGHT = 100000;
    private MigLayout layout;
    private LinkedList<Component> menuComps;
    private LinkedList<Component> imageOptionsComps;
    private LinkedList<Component> boardOptionsComps;
    private boolean isPressable;


    //----------------Constructor----------------------------------------------------------------------------------------------
    public MainMenu() {
        super("Sliding Puzzle");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.isPressable = true;//refers to the image choosing buttons

        this.layout = new MigLayout();
        this.setLayout(this.layout);

        this.menuComps = new LinkedList<>();
        this.imageOptionsComps = new LinkedList<>();
        this.boardOptionsComps = new LinkedList<>();

        this.mainMenuLabel = new OOPLabel(this, "Main Menu");
        this.imageChoiceLabel = new OOPLabel(this, "Choose an Image");
        this.boardChoiceLabel = new OOPLabel(this, "Choose a Board");

        //System.getenv();
        this.startButton = new JButton("Start Game");
        this.exitButton = new JButton("Exit");

        this.catButton = BuildOOPButton("\\PicturesFile\\cat\\cat.jpeg");
        this.cyberButton = BuildOOPButton("\\PicturesFile\\cyber\\cyber.jpeg");
        this.sushiButton = BuildOOPButton("\\PicturesFile\\sushi\\sushi.jpg");
        this.board_3x3 = new JButton("3 x 3");
        this.board_4x4 = new JButton("4 x 4");
        this.board_5x5 = new JButton("5 x 5");

        initMenu();
        initPictureButtons();
        initBoardOptions();

        this.startButton.addActionListener(this);
        this.exitButton.addActionListener(this);
        this.catButton.addActionListener(this);
        this.cyberButton.addActionListener(this);
        this.sushiButton.addActionListener(this);
        this.board_3x3.addActionListener(this);
        this.board_4x4.addActionListener(this);
        this.board_5x5.addActionListener(this);

        compSetFont(this.menuComps,new Font("Console", Font.ITALIC, 36));
        compSetFont(this.imageOptionsComps,new Font("Console", Font.ITALIC, 36));
        compSetFont(this.boardOptionsComps,new Font("Console", Font.ITALIC, 36));
        this.setSize(this.frame_WIDTH, this.frame_HEIGHT);
        this.getContentPane().setBackground(Color.cyan);
        setVisible(true);
    }
    //--------------------------------------------------------------ActionPerformed-------------------------------------------------
    public void actionPerformed(ActionEvent event) {
        if (event.getSource().equals(this.startButton)) {
            startGame();
        } else if (event.getSource().equals(this.exitButton)) {
            exitGame();
        } else if (event.getSource().equals(this.catButton)) {
            if (isPressable) {
                this.pic = "cat";
                chooseBoardSize();
            } else {
                //nothing happens
            }
        } else if (event.getSource().equals(this.cyberButton)) {
            if (isPressable) {
                this.pic = "cyber";
                chooseBoardSize();
            } else {
                //nothing happens
            }
        } else if (event.getSource().equals(this.sushiButton)) {
            if (isPressable) {
                this.pic = "sushi";
                chooseBoardSize();
            } else {
                //nothing happens
            }
        } else if (event.getSource().equals(this.board_3x3)) {
            this.dispose();
            new Game(3, this.pic);
        } else if (event.getSource().equals(this.board_4x4)) {
            this.dispose();
            new Game(4, this.pic);
        } else if (event.getSource().equals(this.board_5x5)) {
            this.dispose();
            new Game(5, this.pic);
        }
    }
    //--------------------------------------------------------------publics-------------------------------------------------
    public void startGame() {
        for (Component c : this.imageOptionsComps) {
            c.setVisible(true);
        }
    }

    public void chooseBoardSize() {
        for (Component c : this.boardOptionsComps) {
            c.setVisible(true);
        }
    }

    public void exitGame() {
        dispose();
    }
    //--------------------------------------------------------------Privates-------------------------------------------------
    private void initMenu() {
        this.menuComps.add(this.mainMenuLabel);
        this.menuComps.add(this.startButton);
        this.menuComps.add(this.exitButton);
        for (Component c : this.menuComps) {
            add(c);
            c.setVisible(true);
        }
        this.layout.setComponentConstraints(this.mainMenuLabel, "pos 50px 25px");
        this.layout.setComponentConstraints(this.startButton, "pos 50px 80px");
        this.layout.setComponentConstraints(this.exitButton, "pos 50px 135px");
    }

    private void initPictureButtons() {
        this.imageOptionsComps.add(this.imageChoiceLabel);
        this.imageOptionsComps.add(this.catButton);
        this.imageOptionsComps.add(this.cyberButton);
        this.imageOptionsComps.add(this.sushiButton);
        for (Component c : this.imageOptionsComps) {
            add(c);
            c.setVisible(false);
        }
        this.layout.setComponentConstraints(this.imageChoiceLabel, "pos 50px 200px");
        this.layout.setComponentConstraints(this.catButton, "pos 50px 250px");
        this.layout.setComponentConstraints(this.cyberButton, "pos 1050px 250px");
        this.layout.setComponentConstraints(this.sushiButton, "pos 2050px 250px");
    }

    private void initBoardOptions() {
        this.boardOptionsComps.add(this.boardChoiceLabel);
        this.boardOptionsComps.add(this.board_3x3);
        this.boardOptionsComps.add(this.board_4x4);
        this.boardOptionsComps.add(this.board_5x5);
        for (Component c : this.boardOptionsComps) {
            add(c);
            c.setVisible(false);
        }
        this.layout.setComponentConstraints(this.boardChoiceLabel, "pos 50px 1300px");
        this.layout.setComponentConstraints(this.board_3x3, "pos 50px 1350px");
        this.layout.setComponentConstraints(this.board_4x4, "pos 50px 1400px");
        this.layout.setComponentConstraints(this.board_5x5, "pos 50px 1450px");
    }

    private JButton BuildOOPButton(String strng) {
        Image img = null;
        String imgPath = strng;
        try {
            img = ImageIO.read(getClass().getResource(imgPath));
        }
        catch (IOException e) {
            final Font myFont = new Font("Console", Font.ITALIC, 36);
            JLabel label = new JLabel("Something went wrong");
            label.setFont(myFont);
            JOptionPane.showMessageDialog(null, label,"Error",JOptionPane.ERROR_MESSAGE);
        }
        img = img.getScaledInstance(1000, 1000, Image.SCALE_DEFAULT);
        JButton btn = new JButton();
        btn.setIcon(new ImageIcon(img));
        return btn;
    }
    private void compSetFont(LinkedList<Component> ll ,Font f) {
        for(Component c: ll ) {
            c.setFont(f);
        }
    }
    //-------------------Main--------------
    public static void main(String[] args) {
      new MainMenu();
    }

}
