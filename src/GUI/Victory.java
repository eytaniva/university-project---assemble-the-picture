package GUI;

import LogicLayer.*;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;


public class Victory extends JFrame implements ActionListener {

    //-----------------Fields----------------
    private JButton backToMenuButton, exitButton;
    private OOPLabel victoryLabel, timeLabel, stepsLabel;
    private final int timesSize = 2;
    private final int frame_WIDTH = 500*timesSize, frame_HEIGHT = 220*timesSize;
    private MigLayout layout;
    private LinkedList<Component> menuComps;
    private Game board;

    //----------------Constructor------------
    public Victory(Game board, int steps, String finishTime) {
        super("Sliding Puzzle");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.board = board;

        this.layout = new MigLayout();
        this.setLayout(this.layout);

        this.menuComps = new LinkedList<>();

        this.victoryLabel = new OOPLabel(this, "Victory!");
        this.stepsLabel = new OOPLabel(this, "Steps: " + steps);
        this.timeLabel = new OOPLabel(this, finishTime);

        this.backToMenuButton = new JButton("Back to Menu");
        this.exitButton = new JButton("Exit");

        initMenu();

        this.backToMenuButton.addActionListener(this);
        this.exitButton.addActionListener(this);

        compSetFont(this.menuComps, new Font("Console", Font.ITALIC, 36));
        this.setSize(this.frame_WIDTH, this.frame_HEIGHT);
        this.getContentPane().setBackground(Color.cyan);
        setVisible(true);
    }

    //------------------Privates--------------------------------------------------------------
    private void initMenu() {
        this.menuComps.add(this.victoryLabel);
        this.menuComps.add(this.stepsLabel);
        this.menuComps.add(this.timeLabel);
        this.menuComps.add(this.backToMenuButton);
        this.menuComps.add(this.exitButton);
        for (Component c : this.menuComps) {
            add(c);
            c.setVisible(true);
        }
        this.layout.setComponentConstraints(this.victoryLabel, "pos 50px 25px");
        this.layout.setComponentConstraints(this.stepsLabel, "pos 330px 25px");
        this.layout.setComponentConstraints(this.timeLabel, "pos 330px 100px");
        this.layout.setComponentConstraints(this.backToMenuButton, "pos 50px 100px");
        this.layout.setComponentConstraints(this.exitButton, "pos 50px 175px");
    }
    private void compSetFont(LinkedList<Component> ll ,Font f) {
        for(Component c: ll ) {
            c.setFont(f);
        }
    }

    //-----------------------------Publics---------------------------------
    public void actionPerformed(ActionEvent event) {
        if (event.getSource().equals(this.backToMenuButton)) {
            this.board.dispose();
            this.dispose();
            goBackToMainMenu();
        }
        else if (event.getSource().equals(this.exitButton)) {
            exitGame();
        }
    }

    public void goBackToMainMenu() {
        this.dispose();
        new MainMenu();
    }

    public void exitGame() {
        this.board.dispose();
        this.dispose();
    }
}
